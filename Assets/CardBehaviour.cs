﻿using UnityEngine;
using System.Collections;

public class CardBehaviour : MonoBehaviour {

// Create textures
	Texture2D parisTexPicto;	// Paris Card pictogram
	Texture2D amsTexPicto;		// Amsterdam Card pictogram
	Texture2D sfTexPicto;		// San Francisco Card pictogram
	Texture2D mysteryTexPicto;	// Back Card pictogram
	Texture2D parisCardTex;		// Paris Card Texture
	Texture2D amsCardTex;		// Amsterdam Card Texture
	Texture2D sfCardTex;		// San Francisco Card Texture
	Texture2D TexCard1;			// Texture Card 1
	Texture2D TexCard2;			// Texture Card 2
	Texture2D TexMatch;			// Texture match beween two cards
	Texture2D TexNoMatch;		// Texture no match between cards

	private bool flipping;		// Card is flipping over
	private bool flipback;		// Card is flipping back
	private int rotAngle;		// Angle of rotation of the card

	int currentCard;			// Index of the current symbol ( Paris , Amsterdam, SF...)
	int card1;					// Index of symbol on card 1
	int card2;					// Index of symbol on card 2

	bool cardMatch;				// Is there a match between two cards
	bool cardNoMatch;			// Is there a no match between two cards

	int[] discovered;			// Discovered cards

	// Use this for initialization
	void Start () {

	// Initialize Textures

		mysteryTexPicto = Resources.Load("Gfx/picto/Carte_mystere_picto") as Texture2D;
		parisTexPicto =  Resources.Load("Gfx/picto/Carte_Paris_picto") as Texture2D;
		amsTexPicto = Resources.Load("Gfx/picto/Carte_Ams_picto") as Texture2D;
		sfTexPicto = Resources.Load("Gfx/picto/Carte_Sf_Picto") as Texture2D;
		parisCardTex =  Resources.Load("Gfx/Carte_Paris") as Texture2D;
		amsCardTex =  Resources.Load("Gfx/Carte_Amsterdam") as Texture2D;
		sfCardTex =  Resources.Load("Gfx/Carte_SanFrancisco") as Texture2D;
		TexMatch = Resources.Load("Gfx/TexMatch") as Texture2D;
		TexNoMatch = Resources.Load("Gfx/TexNoMatch") as Texture2D;
		TexCard1 = new Texture2D(144,200);
		TexCard2 = new Texture2D(144,200);

	// Initialize booleans
		flipping =  false;
		flipback = false;
		rotAngle = 0;
		cardMatch = false;
		cardNoMatch = false;

	// Initialize int
		currentCard = -1;
		card1 = -1;
		card2 = -1;

		discovered = new int[3];


	}
	
	// Update is called once per frame
	void Update () {

// Is there a Match between two cards ?

		if( card1 != -1 && card2 != -1){
			if ( card1 == card2){
				cardMatch = true;
				discovered[discovered.Length] = card1;
			}
			else{
				cardNoMatch = true;
			}
			if(!flipback && rotAngle == 0){
				card1 = -1;
				card2 = -1;

				TexCard1 = mysteryTexPicto;
				TexCard2 = mysteryTexPicto;

			}
			
		}

// Card is flipping back

		if(flipback && rotAngle > 0){
			transform.Rotate(0,0,-2);
			rotAngle-=2;
		}
		else{
			if(rotAngle <=0 ){
				flipback = false;

				cardMatch = false;
				cardNoMatch = false;	
			}
		}


// Card is flipping 
		if(flipping == true && rotAngle < 180){
			transform.Rotate(0,0,2); 	
			rotAngle += 2;
		}
		else{
			if( rotAngle >= 180 ){

//set pictogram's texture
				switch(currentCard){
					case 0:
						if(card1 == -1){
							card1 = currentCard;
							TexCard1 = parisTexPicto;
						}
						else{
							card2 = currentCard;
							TexCard2 = parisTexPicto;
						}
						break;
					case 1:
						if(card1 == -1){
							card1 = currentCard;
							TexCard1 = amsTexPicto;
						}
						else{
							card2 = currentCard;
							TexCard2 = amsTexPicto;
						}
						break;
					case 2:
						if(card1 == -1){
							card1 = currentCard;
							TexCard1 = sfTexPicto;
						}
						else{
							card2 = currentCard;
							TexCard2 = sfTexPicto;
						}
						break;
					default:
						break;
				}
				flipping = false;
				FlipCardBack();
			}
		}
	}

// Flip card function
	void FlipCard(){
		currentCard = Random.Range(0, 3);
		switch(currentCard){
			case 0:
				transform.FindChild("Cube").GetComponent<Renderer>().material.mainTexture = parisCardTex;
				break;
			case 1:
				transform.FindChild("Cube").GetComponent<Renderer>().material.mainTexture = amsCardTex;
				break;
			case 2:
				transform.FindChild("Cube").GetComponent<Renderer>().material.mainTexture = sfCardTex;
				break;
			default:
				break;
		}

		flipping = true;
	}

//Flip back function
	void FlipCardBack(){
		flipback = true;
	}

// GUI Management
	void OnGUI(){
		if( !flipping && !flipback ){
			if(GUI.Button(new Rect(300, 1500, 450, 150), "Flip Card")){
				FlipCard();
			}
		}

		GUI.DrawTexture(new Rect(100,100,144,200), TexCard1);
		GUI.DrawTexture(new Rect(300,100,144,200), TexCard2);

		if(cardMatch){
			GUI.DrawTexture(new Rect(500, 100, 300, 200), TexMatch);
		}
		else{
			if(cardNoMatch)
				GUI.DrawTexture(new Rect(500, 100, 300, 200), TexNoMatch);
		}
	}
}
